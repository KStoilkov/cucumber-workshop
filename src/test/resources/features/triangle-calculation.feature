Feature: Determine the type of triangle
  Triangles can be defined as different types. A triangle with equal sides is a equilateral triangle, with two equal
  sides is called a isosceles triangle and other triangles are called scalene triangles.

  Scenario: A triangle with all equal sides
    Given Input calculator is a available
    When 3 equals sides are inputted
    Then the triangle is qualified as 'equilateral'

  Scenario: A triangle with all equal sides alternative
    Given Input calculator is a available
    When I enter for side 1 value 3
    And I enter for side 2 value 3
    And I enter for side 3 value 3
    Then the triangle is qualified as 'equilateral'